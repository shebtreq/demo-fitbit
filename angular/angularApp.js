var app = angular.module('angularApp', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'app/signup.html',
            controller  : 'mainController'
        })
        .when('/sleep', {
            templateUrl : 'app/sleep.html',
            controller  : 'sleepController'
        });
});

app.controller('mainController', function($scope) {
    $scope.referenceUrl = "/fitbit/auth";
});

app.controller('sleepController', function($scope, $http) {
    $http.get("/fitbit/sleep")
        .then(function(response){
                $scope.sleep = response.data;
            },
            function(error) {
            }
        );

    $http.get("/fitbit/user")
        .then(function(response){
                $scope.user = response.data;
            },
            function(error) {
            }
        );

});