var https = require('https');
var http = require('http');
var fs = require('fs');
var express = require('express');

///////////////////////////
//Setup http and https ports
///////////////////////////
var app = express();
var httpsServer = https.createServer({
    key: fs.readFileSync('sslcerts/server.key'),
    cert: fs.readFileSync('sslcerts/server.crt')
}, app);
httpsServer.listen(3000);
var httpServer = http.createServer(app);
httpServer.listen(8080);

///////////////////////////
//Setup routes
///////////////////////////
app.use('/fitbit', require('./routers/fitbitRouter'));
app.use('/app', require('./routers/angularRouter'));
app.use('/*', require('./routers/defaultRouter'));
//app.listen(3000)


///////////////////////////
//Setup models
///////////////////////////
var models = require('./models/index');