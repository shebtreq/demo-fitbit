module.exports = function(sequelize, DataTypes){
    return sequelize.define('Sleep', {
        totalMinutesAsleep: DataTypes.INTEGER,
        totalSleepRecords: DataTypes.INTEGER,
        totalTimeInBed: DataTypes.INTEGER
    })
};