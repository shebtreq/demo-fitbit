module.exports = function(sequelize, DataTypes){
    return sequelize.define('User', {
        userId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        fullName: DataTypes.STRING,
        accessToken: DataTypes.STRING,
        refreshToken: DataTypes.STRING
    })
};