var fs = require('fs');
var path = require('path');
////////////////
//Fitbit routers
////////////////
var router = require('express').Router();
module.exports = router;

router.get('/angularApp.js', function(req, res, next) {
    res.sendFile(path.join(__dirname, '../angular', 'angularApp.js'));
});

router.get('/:fileName', function(req, res, next) {
    var filePath = path.join(__dirname, '../angular/views/pages/', req.params.fileName);
    if (fs.existsSync(filePath )) {
        res.sendFile(filePath);
    } else {
        next();
    }
});

router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname, '../angular/views/main.html'));
});