var path = require('path');
////////////////
//Fitbit routers
////////////////

var authService = require('../services/fitbitOAuthService');
var apiService = require('../services/fitbitApiService');
var fitbitRouter = require('express').Router();
var response = require('../utils/response');
var models = require('../models/index');

module.exports = fitbitRouter;
fitbitRouter.use(authService.initialize());

fitbitRouter.get(['/auth', '/callback'],  authService.authenticate('fitbit', {
    successRedirect: '/fitbit/success',
    failureRedirect: '/fitbit/failure'
}));

fitbitRouter.get('/success', function(req, res, next) {
    res.redirect('/app#/sleep');
});

fitbitRouter.get('/sleep', function(req, res, next) {
    apiService.getSleepData(function(sleep, error) {
        if (error) {
            response.sendInternalServerError(res, error);
        } else {
            response.sendJson(res, sleep);
        }

    });
});

fitbitRouter.get('/user', function(req, res, next) {
    models.User.findAll().then(function(users) {
        if(users.length > 0) {
            response.sendJson(res, users[0]);
        } else {
            next();
        }
    })
});
