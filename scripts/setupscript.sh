#setup git
sudo apt-get install git
git config --global alias.st status
git config --global alias.lg 'log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short'
git config --global alias.ls 'log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate'
git config --global alias.purr pull --rebase

#setup db
sudo apt-get install mysql-server
mysql -u root -e "create database fitbit";


#setup node
sudo apt-get install node
sudo apt-get install npm
sudo npm install -g forever
npm update