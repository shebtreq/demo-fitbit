///////////////////////////////////////
//Setup Fitbit API Service
///////////////////////////////////////
var Request = require('request');
var models = require('../models/index');

var addAccesTokenToHeader = function(accessToken, headers) {
    return addHeader('Authorization', 'Bearer ' + accessToken, headers);

};

var addHeader = function(key, value, headers) {
    var headers = headers || {};
    headers[key] = value;
    return headers;
};

var fitbitUrl = function(path, userId) {
    return 'https://api.fitbit.com/1/user/'+(userId||'-')+path;
};

var dateString = function(date) {
    return dateString = date.toISOString().substring(0, 10);
};

var fitbitApiService = {
    getSleepData: function (callback) {
        models.User.findAll().then(function(users) {
            if(users.length > 0) {
                var user = users[0];
                //var dateString = dateString(new Date());
                get('/sleep/date/' + '2017-01-05' + '.json', user.accessToken, user.userId, callback);
            } else {
                callback(null, new Error())
            }
        });

    }
};
module.exports = fitbitApiService;

function get(path, accessToken, userId, callback) {
    Request({
        url: fitbitUrl(path, userId),
        method: 'GET',
        headers: addAccesTokenToHeader(accessToken),
        json: true
    }, function(error, response, body) {
        if (error) {
            callback(null, error);
        } else {
            var summary = response.body.summary;
            callback(models.Sleep.build({
                totalMinutesAsleep: summary.totalMinutesAsleep,
                totalSleepRecords: summary.totalSleepRecords,
                totalTimeInBed: summary.totalTimeInBed
            }), null);
        }
    });
}
