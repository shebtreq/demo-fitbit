var FitbitStrategy = require( 'passport-fitbit-oauth2' ).FitbitOAuth2Strategy;
var models = require('../models/index');
var path = require('path');

///////////////////////////////////////
//Setup Fitbit OAuth Service
///////////////////////////////////////

var fitbitOAuthService = require('passport');
var config = require(path.join(__dirname, '..', 'config', 'fitbit-config.json'));

fitbitOAuthService.use(new FitbitStrategy({
    clientID: config.clientID,
    clientSecret: config.clientSecret,
    scope: ['activity','heartrate','location','profile', 'sleep'],
    callbackURL: "http://54.213.223.214:8080/fitbit/callback"
}, function(accessToken, refreshToken, profile, done) {
    done(null, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile
    });
}));

fitbitOAuthService.serializeUser(function(user, done) {
    models.User.create({
        userId: user.profile.id,
        fullName: user.profile._json.user.fullName,
        accessToken: user.accessToken,
        refreshToken: user.refreshToken
    }).then(function(user) {
    },function(err) {
    });
    done(null, user);
});

module.exports = fitbitOAuthService;