var response = {
    sendJson: function (res, model) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(model));
    },
    sendInternalServerError: function (res, err) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify(err));
    }
};
module.exports = response;
